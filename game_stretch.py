from random import randint

name = input("Hi! What is your name?\n")

upper_month = 12
lower_month = 1
upper_year = 2004
lower_year = 1924

month_names = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]

for current_guess in range(1,6):
    month= randint(lower_month, upper_month)
    year = randint(lower_year, upper_year)
    day = randint(1, 31)

    print("Guess", current_guess, " :", name, "were you born on",
        month_names[month], day, "/", year, "?")

    answer = input("yes, later, or earlier?\n")

    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "later" and current_guess != 5:
        print("Drat! Lemme try again!")
        lower_year = year 
    elif answer == "earlier" and current_guess != 5:
        print("Drat! Lemme try again!")
        upper_year = year 
    else:
        print("I have other things to do. Good Bye.")

