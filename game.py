from random import randint

name = input("Hi! What is your name? ")


for current_guess in range(1,6):

    month= randint(1, 12)
    year = randint(1924, 2004)

    print("Guess", current_guess, " :", name, "were you born in",
        month, "/", year, "?")

    answer = input("yes or no? ")

    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no" and current_guess != 5:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good Bye.")

